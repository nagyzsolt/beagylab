/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC16F18857
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"

/*
                         Main application
 */

void myADCC_InterruptHandler(void);

void myADCC_InterruptHandler(void) {
    adc_result_t result = ADCC_GetConversionResult();

    /* VDD = 1024 | 0.75 * VDD = 768 | 0.5 * VDD = 512 | 0.25 * VDD = 256 | VSS = 0 */
    if (768U < result) {
        LATC = 0xf0U | (LATC & 0x0fU);
    } else if (512U < result) {
        LATC = 0x70U | (LATC & 0x0fU);
    } else if (256U < result) {
        LATC = 0x30U | (LATC & 0x0fU);
    } else {
        LATC = 0x10U | (LATC & 0x0fU);
    }
}
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    
    //adc_result_t result;    
    //ADCC_DisableContinuousConversion();

    ADCC_SetADIInterruptHandler(myADCC_InterruptHandler);
    ADCC_StartConversion(channel_ANA1);
    while (1)
    {
        //result = ADCC_GetSingleConversion(channel_ANA1);
        
        /* VDD = 1024 | 2V = 621 | 1V = 310 | VSS = 0 */
        //if(621U <= result){
        //    LATC |= (1<<4U);
        //}
        //else if (310U >= result){
        //    LATC &= ~(1<<4U);
        //}
    }
}
/**
 End of File
*/