/*
 * File:   main.c
 * Author: Mark
 *
 * Created on September 20, 2017, 10:18 PM
 */


#include <xc.h>

void main(void) {
    /*************************************
     *          CONFIG
     *************************************/
    /* OSC config*/
    // NOSC HFINTOSC; NDIV 1; 
    OSCCON1 = 0x60;
    // HFFRQ 4_MHz; 
    OSCFRQ = 0x02;
    
    /* IO config */
    //Reset IOs
    LATA = 0x00;    
    LATB = 0x00;    
    LATC = 0x00;    
    // Set LEDs to output
    TRISC = 0x0F;
    // Disable ANALOG func. on LEDs and Buttons
    ANSELC = 0x0F;
    ANSELA = 0x3F;
    
    /* Timer2 Config*/
    T2CON = 0xb4U; // TODO Calculate Postscaler and Prescaler
    T2PR = 154U;// TODO calculate period register
    T2CLKCON = 0x04U;// TODO select clock source    
    T2TMR = 0x00;// Reset Timer 2;
    
    PIR4bits.TMR2IF = 0;// Clearing IF flag.    
    T2CONbits.T2ON = 1;// Start timer    
    PIE4bits.TMR2IE = 1;// Enable Timer 2 int
    
    /* Timer2 Config*/
    T4CON = 0xc4U; // TODO Calculate Postscaler and Prescaler
    T4PR = 155U;// TODO calculate period register
    T4CLKCON = 0x04U;// TODO select clock source    
    T4TMR = 0x00;// Reset Timer 2;
    
    PIR4bits.TMR4IF = 0;// Clearing IF flag.    
    T4CONbits.T4ON = 1;// Start timer    
    PIE4bits.TMR4IE = 1;// Enable Timer 2 int
    
    INTCONbits.PEIE = 1; // Enable Peripheral int
    INTCONbits.GIE = 1; // Enable Global interrupt
    /*************************************
     *          Main program
     *************************************/
    while(1){
        if (PORTAbits.RA7){
            PIE4bits.TMR2IE = 1;
        }else{
            PIE4bits.TMR2IE = 0;
        }
        if (PORTAbits.RA6){
            PIE4bits.TMR4IE = 1;
        }else{
            PIE4bits.TMR4IE = 0;
        }
    }

    return;
}
