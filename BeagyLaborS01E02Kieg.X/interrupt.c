/*
 * 2. Gyakorlat V. feladat megvalósítása
 */
#include <xc.h>
void __interrupt() INTERRUPT_InterruptManager (void)
{
    if(PIR4bits.TMR2IF == 1 && PIE4bits.TMR2IE == 1)
    {
		PIR4bits.TMR2IF = 0; // Clear IF
		LATCbits.LATC5 ^= 1; //invert LED2   
    }
	
    if(PIR4bits.TMR4IF == 1 && PIE4bits.TMR4IE == 1)
    {
		PIR4bits.TMR4IF = 0; // Clear IF
        LATCbits.LATC6 = ~LATCbits.LATC6; //invert LED3 
    }
}